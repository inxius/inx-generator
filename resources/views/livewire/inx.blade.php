<div class="m-0 p-0 w-full flex justify-center">
    <div class="w-fit flex flex-col gap-8 justify-center">
        <div class="grid grid-cols-1 md:grid-cols-2 justify-items-center gap-8">
            <div class="relative max-w-md flex flex-col justify-center items-center border-solid border-2 p-4 border-stone-500 rounded-md bg-stone-100">
                <div class="absolute -top-3 bg-stone-700 self-center border-solid border-2 border-stone-700 rounded-md">
                    <p class="text-xl px-2 text-sky-50">Data</p>
                </div>
                <div class="flex flex-row gap-x-1 gap-y-6 pt-4 flex-wrap justify-center items-center">  
                    <div>
                        <input wire:model='requestData' value="email" type="checkbox" id="choose-me-email" class="peer hidden" />
                        <label for="choose-me-email" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> E-Mail </label>
                    </div>

                    <div>
                        <input wire:model='requestData' value="username" type="checkbox" id="choose-me-username" class="peer hidden" />
                        <label for="choose-me-username" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> Username </label>
                    </div>

                    <div>
                        <input wire:model='requestData' value="domain" type="checkbox" id="choose-me-domain" class="peer hidden" />
                        <label for="choose-me-domain" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> Domain </label>
                    </div>

                    <div>
                        <input wire:model='requestData' value="nik" type="checkbox" id="choose-me-nik" class="peer hidden" />
                        <label for="choose-me-nik" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> NIK </label>
                    </div>

                    <div>
                        <input wire:model='requestData' value="name" type="checkbox" id="choose-me-name" class="peer hidden" />
                        <label for="choose-me-name" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> Nama </label>
                    </div>

                    <div>
                        <input wire:model='requestData' value="alamat" type="checkbox" id="choose-me-alamat" class="peer hidden" />
                        <label for="choose-me-alamat" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> Alamat </label>
                    </div>

                    <div>
                        <input wire:model='requestData' value="telp" type="checkbox" id="choose-me-telp" class="peer hidden" />
                        <label for="choose-me-telp" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> No Telp </label>
                    </div>

                    <div>
                        <input wire:model='requestData' value="tanggal" type="checkbox" id="choose-me-tanggal" class="peer hidden" />
                        <label for="choose-me-tanggal" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> Tanggal </label>
                    </div>

                    <div>
                        <input wire:model='requestData' value="waktu" type="checkbox" id="choose-me-waktu" class="peer hidden" />
                        <label for="choose-me-waktu" class="select-none cursor-pointer rounded-lg bg-stone-500 py-3 px-6 font-bold text-sky-50 transition-colors duration-200 ease-in-out peer-checked:bg-stone-700"> Waktu </label>
                    </div>
                </div>
            </div>
            
            <div class="flex flex-col gap-8">
                <div class="relative max-w-md h-fit flex flex-col justify-start items-center border-solid border-2 p-4 border-stone-500 rounded-md bg-stone-100">
                    <div class="absolute -top-3 bg-stone-700 self-center border-solid border-2 border-stone-700 rounded-md">
                        <p class="text-xl px-2 text-sky-50">Export To</p>
                    </div>
                    <div class="flex flex-row gap-4 pt-4 flex-wrap justify-center items-center">
                    
                        <div class="grid max-w-sm grid-cols-3 gap-2 bg-stone-500 rounded-x p-0 rounded-md">
                            <div>
                                <input wire:model='exportTo' type="radio" name="option" id="excel" value="excel" class="peer hidden" checked />
                                <label for="excel" class="block cursor-pointer select-none rounded-md p-2 text-center text-sky-50 peer-checked:bg-stone-700 peer-checked:font-bold peer-checked:text-white">Excel</label>
                            </div>
                    
                            <div>
                                <input wire:model='exportTo' type="radio" name="option" id="csv" value="csv" class="peer hidden" />
                                <label for="csv" class="block cursor-pointer select-none rounded-md p-2 text-center text-sky-50 peer-checked:bg-stone-700 peer-checked:font-bold peer-checked:text-white">CSV</label>
                            </div>
    
                            <div>
                                <input wire:model='exportTo' disabled type="radio" name="option" id="json" value="json" class="peer hidden" />
                                <label for="json" class="block cursor-pointer select-none rounded-md p-2 text-center text-sky-50 peer-checked:bg-stone-700 peer-checked:font-bold peer-checked:text-white">JSON</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex flex-col items-center justify-center">
                    <form wire:submit.prevent='generateData'>
                        <div class="relative max-w-sm flex flex-col justify-start items-center border-solid border-2 p-4 border-stone-500 rounded-md bg-stone-100">
                            <div class="absolute -top-3 bg-stone-700 self-center border-solid border-2 border-stone-700 rounded-md">
                                <p class="text-xl px-2 text-sky-50">Rows</p>
                            </div>
                            <div class="pt-4 flex flex-row gap-4">
                                <input wire:model='rowsRecord' type="number" placeholder="Rows Record" min="1" max="1000" class="max-w-sm py-2 px-4 border-solid border-2 border-stone-500 rounded-md">
                                <button type="submit" class="bg-stone-500 hover:bg-stone-700 py-2 px-3 rounded-md hover:scale-x-105"> <p class="text-sm md:text-lg font-medium text-sky-50">Generate</p> </button>
                            </div>
                        </div>
        
                    </form>
                    @error('rowsRecord')
                        <span class="pl-1 italic font-bold text-xs text-red-700">* {{$message}}</span>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>
