<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel = "icon" href = {{'gear.svg'}}
        type = "image/x-icon">
    <title>INX-Generator</title>
    @vite('resources/css/app.css')
    @livewireStyles
</head>
<body class="">
    <div class="m-0 bg-stone-300 py-8 px-4 md:py-16 md:px-8 flex flex-col justify-center items-center gap-2">
        <p class="text-5xl md:text-7xl text-stone-700">INX Generator</p>
        <p class="text-xl md:text-3xl font-light text-stone-600">Generate test data. Quickly.</p>
    </div>

    <div class="py-8 px-4">
        <livewire:inx />
    </div>
    @livewireScripts
</body>
</html>