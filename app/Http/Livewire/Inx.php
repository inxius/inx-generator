<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Faker\Factory as Faker;
use Maatwebsite\Excel\Excel;
use App\Exports\DummyExport;
use App\Actions\GenerateHead;
use App\Actions\GenerateData;

class Inx extends Component
{
    public $requestData = [];

    public $exportTo;
    public $rowsRecord;

    public function hydrate() {
        //
    }

    public function render()
    {
        return view('livewire.inx');
    }

    public function generateData(Excel $excel, GenerateHead $actionHead, GenerateData $actionData) {
        $this->validate([
            'rowsRecord' => 'required|min:1|max:1000',
        ]);
        
        $heading = $actionHead->execute($this->requestData);
        $data = $actionData->execute($this->requestData, $this->rowsRecord);

        $faker = Faker::create('id_ID');

        $fileNime = $faker->lexify('inx-generator-?????????');
        $export = new DummyExport($data, $heading);

        if ($this->exportTo == 'csv') {
            return $excel->download($export, $fileNime.'.csv', \Maatwebsite\Excel\Excel::CSV);
        } else {
            return $excel->download($export, $fileNime.'.xlsx', \Maatwebsite\Excel\Excel::XLSX);
        }
    }
}
