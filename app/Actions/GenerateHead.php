<?php

namespace App\Actions;

class GenerateHead
{
    public function execute(array $requestData) : array {
        $head = [];

        in_array('nik', $requestData) ? array_push($head, 'nik') : '';
        in_array('email', $requestData) ? array_push($head, 'email') : '';
        in_array('name', $requestData) ? array_push($head, 'name') : '';
        in_array('username', $requestData) ? array_push($head, 'username') : '';
        in_array('domain', $requestData) ? array_push($head, 'domain') : '';
        in_array('alamat', $requestData) ? array_push($head, 'alamat') : '';
        in_array('telp', $requestData) ? array_push($head, 'telp') : '';
        in_array('tanggal', $requestData) ? array_push($head, 'tanggal') : '';
        in_array('waktu', $requestData) ? array_push($head, 'waktu') : '';

        return $head;
    }
}