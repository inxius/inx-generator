<?php

namespace App\Actions;

use Faker\Factory as Faker;

class GenerateData
{
    public function execute(array $requestData, int $maxRow) : array {
        $exportData = [];
        $faker = Faker::create('id_ID');

        for ($i=0; $i < $maxRow; $i++) { 
            $temp = array();

            in_array('nik', $requestData) ? $temp += ['nik' => $faker->nik()] : '';
            in_array('email', $requestData) ? $temp += ['email' => $faker->email()] : '';
            in_array('name', $requestData) ? $temp += ['name' => $faker->name()] : '';
            in_array('username', $requestData) ? $temp += ['username' => $faker->userName()] : '';
            in_array('domain', $requestData) ? $temp += ['domain' => $faker->domainName()] : '';
            in_array('alamat', $requestData) ? $temp += ['alamat' => $faker->address()] : '';
            in_array('telp', $requestData) ? $temp += ['telp' => $faker->phoneNumber()] : '';
            in_array('tanggal', $requestData) ? $temp += ['tanggal' => $faker->date()] : '';
            in_array('waktu', $requestData) ? $temp += ['waktu' => $faker->time()] : '';

            array_push($exportData, $temp);
        }

        return $exportData;
    }
}