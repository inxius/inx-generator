<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DummyExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;

    private $dummy;
    private $head;

    public function __construct(array $dummy, array $head)
    {
        $this->dummy = $dummy;
        $this->head = $head;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect($this->dummy);
    }

    public function headings() : array {
        return $this->head;
    }
}
